from werkzeug.wrappers import Request, Response

import os
from json import loads
from subprocess import Popen, DEVNULL


def application(environ, start_response):
    request = Request(environ)
    # text = 'Hello %s!' % request.args.get('name', 'World')

    if ~request.content_type.lower().find('json') and request.content_length:
        data = loads(request.data.decode('utf-8'))
        if data['type'] in ['youtube', 'twitch']:
            print('mpv {}'.format(data['url']))
            Popen(['mpv', data['url']], stdout=DEVNULL, stderr=DEVNULL)
    response = Response(text, mimetype='text/plain')
    return response(environ, start_response)
