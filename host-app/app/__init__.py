from .cors import CORSMiddleware
from .app import application

def create_app():
    return CORSMiddleware(application, "*")