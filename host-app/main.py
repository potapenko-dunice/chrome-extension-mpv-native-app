#!/usr/bin/env python3
from app import create_app

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple('127.0.0.1', 1337, create_app(), use_debugger=True, use_reloader=False)
